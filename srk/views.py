from django.shortcuts import render
from rest_framework import viewsets
from .models import Employ
from .serializers import EmploySerializer
# Create your views here.


class EmployViewSet(viewsets.ModelViewSet):
    queryset = Employ.objects.all()
    serializer_class = EmploySerializer
