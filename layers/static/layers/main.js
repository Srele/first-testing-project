function init() {
    proj4.defs("EPSG:3907", "+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=5500000 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +units=m +no_defs");
    ol.proj.proj4.register(proj4);
    map = new ol.Map({
        view: new ol.View({
            center: ol.proj.fromLonLat([18.6735, 44.5375], 'EPSG:3907'),
            zoom: 7,
            projection: 'EPSG:3907'
        }),
        target: 'map'
    });

    const basicTileLayer = new ol.layer.Tile({
        source: new ol.source.OSM()
    });

    const anotherLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: 'https://sreckokuz.github.io/hosting-/map3.geojson',
            format: new ol.format.GeoJSON()
        }),
        visibel: true,
        name: 'firstLayer',
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: [255, 255, 255, 100, 0.5]
            }),
            stroke: new ol.style.Stroke({
                color: [55, 55, 55, 55, 0.7],
                width: 4
            }),
            image: new ol.style.Icon({
                src: 'https://sreckokuz.github.io/hosting-/icons8-marker-64.png',
                scale: 0.5,
                opacity: 0.7
            })
        })
    });

    const EUstyle = function (feature) {
        let geometryType = feature.getGeometry().getType();
        console.log(feature.get('color'));
    }

    const secondVector = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: 'https://sreckokuz.github.io/hosting-/map4.geojson',
            format: new ol.format.GeoJSON()
        }),
        style: EUstyle
    });

    map.addLayer(basicTileLayer);
    map.addLayer(secondVector);
    map.addLayer(anotherLayer);



    const lineDraw = new ol.interaction.Draw({
        type: 'Polygon',
    });

    lineDraw.on("drawstart", function (e) {
        console.log('start draw');
    });


    lineDraw.on("drawend", function (e) {
        console.log('drawind ended');
        parser = new ol.format.GeoJSON();
        const coords = parser.writeFeatures([e.feature]);
        console.log(coords);
        setTimeout(function () {
            $('#myModal').modal('toggle');
        }, 1000);
        $("#submitForm").click(function () {
            //console.log("srkomannnn");
            $.ajax({
                type: 'POST',
                url: '/layer/saveElements/',
                data: {
                    //'allFormData': data, // from form
                    'allFormData': $('#test').serialize(), // from form
                    'geoJsonData': coords
                },
                dataType: "json",
                success: function () {
                    $('#message').html("<h2>Contact Form Submitted!</h2>")
                }
            });
            console.log('slobodannn');
        });

    })


    const selectElement = new ol.interaction.Select();
    selectElement.on('select', function (e) {
        if (e.selected.length > 0) {
            console.log(e.selected[0]['values_'].name);
            if (e.selected[0].getGeometry().getType() === 'Point' && e.selected[0]['values_'].name === 'Point 2') {
                console.log(selectElement.getFeatures());


                e.selected[0].setStyle([
                    new ol.style.Style({
                        image: new ol.style.Circle({
                            fill: new ol.style.Fill({
                                color: [255, 255, 255, 50, 1]
                            }),
                            stroke: new ol.style.Stroke({
                                color: [55, 55, 55, 55, 1],
                                width: 3
                            }),
                            radius: 12
                        })
                    })
                ]);
            } else if (e.selected[0].getGeometry().getType() === 'LineString') {
                e.selected[0].setStyle([
                    new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: [22, 22, 22, 22, 1],
                            width: 10
                        })
                    })
                ]);
            } else {
                e.selected[0].setStyle(
                    [
                        new ol.style.Style({
                            fill: new ol.style.Fill({
                                color: [12, 12, 12, 100, 1],
                            }),
                            stroke: new ol.style.Stroke({
                                color: [12, 12, 12, 100, 1],
                                width: 5
                            })
                        })
                    ])
            }
        }
    })
    map.addInteraction(selectElement);



    map.addInteraction(lineDraw);


}

window.onload = init;

