from django.urls import path
from layers import views

urlpatterns = [
    path('', views.display_map, name='displaymap'),
    path('saveElements/', views.saveElements, name='save_element'),
    path('cities/', views.display_cities, name='cities'),
    path('drawing/', views.drawing_cities, name='draw'),
    path('error/', views.error_handling, name='error'),





]
