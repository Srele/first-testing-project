from django import forms
from . models import Polygonion


class PolyForm(forms.ModelForm):
    class Meta:
        model = Polygonion
        fields = ['name', 'age']
